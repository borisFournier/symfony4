<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\MessageGenerator;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Task;
use App\Form\TaskType;
use App\Service\MarkdownHelper;
use App\Updates\TestEmailManager;

class ArticleController extends AbstractController
{    
    /**
     * @Route("/", name="app_homepage")
     */
    public function homepage()
    {
        
        return $this->render('article/homepage.html.twig');
    }

    /**
     * @Route("/taskForm", name="taskForm")
     */
    public function taskForm(Request $request)
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();
    
            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            // $entityManager = $this->getDoctrine()->getManager();
            // $entityManager->persist($task);
            // $entityManager->flush();
    
            return $this->render('article/homepage.html.twig');
        }

        return $this->render('article/taskForm.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/service", name="app_service")
     */
    public function service(MessageGenerator $messageGenerator)
    {
        $message = $messageGenerator->getHappyMessage();
        $this->addFlash('success', $message);

        return $this->render('article/service.html.twig');
    }

    /**
     * @Route("/news/{slug}", name="show_article")
     */
    public function show($slug, MarkdownHelper $markdownHelper)
    {
        $comments = ['bonjour','salut','au revoir',];
        $this->addFlash(
            'notice',
            'Tu vas bien sur news'
        );   
        
        $articleContent = "Le **bacon** est simplement du faux texte employé dans la composition et la mise en page avant impression. Le [Lorem Ipsum](https://knpuniversity.com/screencast/symfony-fundamentals/markdown-bundle#play) est le faux texte standard de l'imprimerie depuis les années **1500**, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker."; 

        $articleContent = $markdownHelper->parse($articleContent); 


        return $this->render('article/article.html.twig',[
            'title' => ucwords(str_replace('-', '',$slug)),
            'comments' => $comments,
            'articleContent' => $articleContent
        ]);
    }

    /**
     * @Route("/email", name="app_testEmail")
     */
    public function testEmail(TestEmailManager $testEmailManager)
    {
        $email = $testEmailManager->sendEmail();

        return $this->render('article/testEmail.html.twig', ['email' => $email]);
    }

    

}