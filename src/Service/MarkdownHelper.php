<?php

namespace App\Service;

use Michelf\MarkdownInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Psr\Log\LoggerInterface;

class MarkdownHelper
{
    private $cache;
    private $markdown;
    private $logger;

    public function __construct(MarkdownInterface $markdown, AdapterInterface $cache, LoggerInterface $logger)
    {
        $this->markdown = $markdown;
        $this->cache = $cache;
        $this->logger = $logger;
    }


    public function parse(string $source):string
    {
        if(stripos($source, 'bacon') !== false){
            $this->logger->info('they\'re talking about bacon again');
        }

        $item = $this->cache->getItem('markdown_'.md5($source));

        if(!$item->isHit()){
            $item->set($this->markdown->transform($source));
            $this->cache->save($item);
        }
        return $source = $item->get();
        
    }

}