<?php

namespace App\Updates;

use App\Service\MessageGenerator;

class TestEmailManager
{
    private $mailer;
    private $messageGenerator;

    public function __construct(MessageGenerator $messageGenerator, \Swift_Mailer $mailer) 
    {

        $this->messageGenerator = $messageGenerator;
        $this->mailer = $mailer;

    }

    public function sendEmail()
    {
        $message = $this->messageGenerator->getHappyMessage();

        $email = (new \Swift_Message('test d\'envoi d\'email!!'))
            ->setFrom('fboris73@gmail.com')
            ->setTo('fboris73@gmail.com')
            ->addPart(
                'On vient d\'envoyer un email avec ce message mec : ' . $message
        );
        return $this->mailer->send($email);

    }

}
